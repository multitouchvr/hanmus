# PJI Théo Serra

Ce projet correspond au Projet Individuel (PJI) de Théo SERRA encadré par Florent BERTHAUT.
Il s'agit de la continuation du Projet de Fin d'Etudes (PFE) d'Antoine NOLLET, encadré par Florent BERTHAUT et Thomas PIETRZAK.

### Idées d'ajouts au projet

<b>Deuxième technique d'interaction : </b>

Pour la première interaction pensée par Antoine Nollet, un cône permet de sélectionner différents objets dans la scène 3D.
La position du cône par rapport à un objet permet d'attribuer des valeur de gains plus ou moins élevés au capteurs piezos de la manette.
Ainsi si l'objet se trouve à droite du cône, les capteurs piezos à droite de la manette auront un gain plus élevés que ceux à gauche.

![schema première interaction](./Data/schema_antoine1.png)

Cette deuxième technique se base sur le même principe de sélection avec par un cône mais permettra une interaction différente avec la manette.</br>
Cette fois-ci lorsque le cône est en intersection avec un objet, tous les capteurs auront un gain identique mais le son joué lors d'un vibration sera différent.
Ainsi, chaque objet possède sa propre interaction et un son différent qui dépend de la zone d'intersection entre le cône et lui même et de où l'utilisateur agit sur la manette.

![Deuxième interaction](./Data/schema_inter2.png)

<b>Amélioration des visualisation :</b></br>

Le but est de créer un retour visuel vers l'utilisateur lorsqu'il utilise la manette.</br>
La sélection des éléments au sein de la scène 3D se fait grâce à un cône de couleur unie.
Cette couleur pourrait être modifée lorsque qu'une vibration est ressentie par les capteurs piezos.
Ainsi une vibration sur le capteur en haut à droite changerait la couleur de la même zone du cône.
Un shader de Godot permettrait ce changement.

![Visualisation première interaction](./Data/schema_antoine2.png)

Pour la deuxième interaction décrite plus haut, la visualisation se ferait au niveau de l'intersection entre le cône et l'objet.
Seule cette zone d'intersection changerait de couleur et on pourrait ajouter une visualisation des capteurs piezos dans cette même zone.

![Visualisation deuxième interaction](./Data/visu_inter2.png)

### Ajouts réalisés

Toutes les modifications et ajouts ont été fais dans le script attaché à l'objet spatial <i>ConeSelect2</i>

La visualisation de la deuxième méthode a été implémentée.</br>
Lorsque le cône de sélection entre en collision avec un objet, un plan noir représentant la manette s'affiche.
Le plan fait la taille de la surface de collision et est situé au centre de celle-ci.
Quand l'utilisateur utilise la manette, les gains des capteurs piezos sont envoyés au shader du plan qui modifiera la couleur de la surface comme expliqué plus haut.
Les différents plans sont affichés grâce à un multimesh <i>VisuPlans</i> enfant de <i>ConeSelect2</i>

Si l'objet se situe à une certaine distance proche de l'utilisateur, le cône de sélection disparaît et un plan s'affiche au niveau de la main.
Ce plan suivra les mouvements fait avec la manette et sa couleur changera comme pour tous les autres plans (à tester)

Un identifiant <i>tech_id</i> permet de changer la méthode de visualisation.
Le mettre à 1 permet d'obtenir ce qui est décrit ci-dessus.
Le passer à 0 affichera un plan à l'extrémité du cône pour avoir une visualisation correspondant à la technique d'interaction d'Antoine Nollet.

Enfin, un script attaché à l'objet spatial <i>HandSelect</i> permet à l'utilisateur d'attraper les objets en pinçant son index avec son pouce.
Cela est possible avec l'utilisation de l'objet <i>RightHandSkeleton</i> enfant de <i>ARVRControllerD</i> qui est un squelette de la main droite.
D'autres interactions peuvent être pensées en suivant la même méthode.
